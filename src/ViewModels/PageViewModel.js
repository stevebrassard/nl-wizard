import * as ko from 'knockout';
export default class PageViewModel {
    constructor() {
        this.enableNext = ko.observable(false);
        this.enablePrevious = ko.observable(false);
    }

    toggleNext() {
        this.enableNext(!this.enableNext());
    }

    togglePrev() {
        this.enablePrevious(!this.enablePrevious());
    }
}