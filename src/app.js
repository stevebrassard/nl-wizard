import ko from 'knockout';
import PageViewModel from './ViewModels/PageViewModel';

ko.bindingHandlers.wizard = {
    init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
        // Called when the binding is first applied to an element  
        var params = valueAccessor();
        var wizard = $(element).steps({
            enableCancelButton: false,
            enableFinishButton: false
            ,onStepChanging: function (event, currentIndex, newIndex) { 
                return ko.unwrap(params.enableNext);
            }
            ,onStepChanged: function (event, currentIndex, priorIndex) { 
                if(ko.unwrap(params.enableNext) === true)
                {
                    $(element).find("div.actions>ul>li:nth-child(2)").removeClass("disabled").attr("aria-disabled", "false")
                } else {
                    $(element).find("div.actions>ul>li:nth-child(2)").addClass("disabled").attr("aria-disabled", "true");
                }                
            }            
        });  
        var prev = $(element).find("div.actions>ul>li:nth-child(1)");
        var next = $(element).find("div.actions>ul>li:nth-child(2)");   
        if(ko.unwrap(params.disableNavigationOnInit)){ 
            next.addClass("disabled").attr("aria-disabled", "true");
            prev.addClass("disabled").attr("aria-disabled", "true");
        }         
    },
    update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
        // Called once when the binding is first applied to an element, and again whenever any observables/computeds that are accessed change  
        var params = valueAccessor();
        var prev = $(element).find("div.actions>ul>li:nth-child(1)");
        var next = $(element).find("div.actions>ul>li:nth-child(2)");
        ko.unwrap(params.enableNext) === true ? next.removeClass("disabled").attr("aria-disabled", "false") : next.addClass("disabled").attr("aria-disabled", "true");
        if($(element).find(".steps ul li.current").hasClass('error')) {
            $(element).find('.steps ul li.current').removeClass('error')
        }
    }
};

ko.applyBindings(new PageViewModel(), document.getElementById('root')); //represents some VM the componets belong to
